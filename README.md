# TLS Demo

## Run
mvn spring-boot:run

## Links
https://localhost:8081/api/test


## Test

curl -i -k -X GET https://localhost:8081/api/test -H "Content-Type: application/json"


