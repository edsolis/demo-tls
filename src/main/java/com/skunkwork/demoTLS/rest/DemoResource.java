package com.skunkwork.demoTLS.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DemoResource {

    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public ResponseEntity<?> greeting() {
        return ResponseEntity.ok("https request!");
    }
}
